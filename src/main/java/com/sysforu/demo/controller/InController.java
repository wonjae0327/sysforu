package com.sysforu.demo.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;

@Controller
@RequestMapping(value = "/in")
public class InController {

	@GetMapping("/index")
	public String index(HttpServletRequest request, Model model) {
		return "in/index";
	}
	
	@GetMapping("/regist")
	public String regist(HttpServletRequest request, Model model) {
		model.addAttribute("survFoot", new Gson().toJson(request.getAttribute("surv")));
		
		return "in/regist";
	}
	
}
