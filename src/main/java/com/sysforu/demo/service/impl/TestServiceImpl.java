package com.sysforu.demo.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sysforu.demo.mapper.TestMapper;
import com.sysforu.demo.service.TestService;

@Service
public class TestServiceImpl implements TestService {
	@Autowired
	TestMapper mapper;

	@Override
	public List<Map<String, Object>> selectVisitorT() throws Exception {
		return mapper.selectVisitorT();
	}

}
