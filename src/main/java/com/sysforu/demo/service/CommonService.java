package com.sysforu.demo.service;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.sysforu.demo.vo.InfoVO;
import com.sysforu.demo.vo.InitVO;

public interface CommonService {

	Map<String, Object> getInit();
	Map<String, Object> getInfo();
	Map<String, Object> getSurv();
	List<Map<String, Object>> selectColumnT();
	List<Map<String, Object>> selectSurvT();
	String getSurvCode();
	Map<String, Object> getGubun();
	Map<String, Object> setInit(InitVO init);
	Map<String, Object> setInfo(InfoVO info);
	Map<String, Object> setGubun(HttpServletRequest request);
	Map<String, Object> setSurv(HttpServletRequest request);
	Map<String, Object> setNdata(Map<String, Object> entity);
	Map<String, Object> selectExhibitT(Map<String, Object> initMap);
	Map<String, Object> duplicateCheck(Map<String, Object> entity);
}
