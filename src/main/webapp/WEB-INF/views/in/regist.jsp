<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="p-1">
	<form id="registFrm" onsubmit="return false;">
		<div class="card">
		
			<!-- 이용 약관 동의 -->
			<fieldset>
				<div class="card-body text-center bg-primary text-white">
					<h5 class="card-title fw-bold">
						<spring:message code="regist.title" />
					</h5>
				</div>
				<div class="card-body bg-secondary bg-opacity-10">
					<spring:message code="regist.notice" arguments="${initVO.titleStr };${initVO.organizerName };" htmlEscape="false" argumentSeparator=";" />
				</div>
				<div class="card-body text-center">
					<input type="checkbox" id="agreeA" name="agreeA" value="Y" class="form-check-input">
					<label for="agreeA" class="form-label fw-bold">
						<spring:message code="regist.agree.all" />
					</label>
				</div>
				<div class="card-body">
					<input type="checkbox" id="agree" name="agree" value="Y" class="form-check-input">
					<label for="agree" class="form-label fw-bold">
						<spring:message code="regist.agree.label" />
					</label>
					<textarea class="form-control" rows="3" readonly>
<spring:message code="regist.collection.contents" arguments="${not empty initVO.exhibitionSiteUrl ? initVO.exhibitionSiteUrl : ' ' }" /></textarea>
				</div>
				<div class="card-body">
					<input type="checkbox" id="agree2" name="agree2" value="Y" class="form-check-input">
					<label for="agree2" class="form-label fw-bold">
						<spring:message code="regist.collection.label" />
					</label>
					<textarea class="form-control" rows="3" readonly>
<spring:message code="regist.provide.contents" /></textarea>
				</div>
				<div class="card-body">
					<input type="checkbox" id="EMY" name="EMY" value="Y" class="form-check-input">
					<label for="EMY" class="form-label fw-bold">
						<spring:message code="regist.receive.email.label" />
					</label>
					<div class="table-responsive">
						<table class="table">
							<thead class="text-center table-secondary">
								<tr class="border border-start-0 border-end-0 border-secondary-subtle">
									<th class="w-25">
										<spring:message code="regist.collection.head.purpose" />
									</th>
									<th class="w-25 border border-secondary-subtle">
										<spring:message code="regist.collection.head.item" />
									</th>
									<th class="w-25">
										<spring:message code="regist.collection.head.period" />
									</th>
								</tr>
							</thead>
							<tbody>
								<tr class="align-middle text-center">
									<td>
										<spring:message code="regist.collection.email.body.purpose" />
									</td>
									<td class="border">
										<spring:message code="regist.collection.email.body.item" />
									</td>
									<td>
										<spring:message code="regist.collection.email.body.period" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-body">
					<input type="checkbox" id="MBY" name="MBY" value="Y" class="form-check-input">
					<label for="MBY" class="form-label fw-bold">
						<spring:message code="regist.receive.text.message.label" />
					</label>
					<div class="table-responsive">
						<table class="table">
							<thead class="text-center table-secondary">
								<tr class="border border-start-0 border-end-0 border-secondary-subtle">
									<th class="w-25">
										<spring:message code="regist.collection.head.purpose" />
									</th>
									<th class="w-25 border border-secondary-subtle">
										<spring:message code="regist.collection.head.item" />
									</th>
									<th class="w-25">
										<spring:message code="regist.collection.head.period" />
									</th>
								</tr>
							</thead>
							<tbody>
								<tr class="align-middle text-center">
									<td>
										<spring:message code="regist.collection.number.body.purpose" />
									</td>
									<td class="border">
										<spring:message code="regist.collection.number.body.item" />
									</td>
									<td>
										<spring:message code="regist.collection.number.body.period" />
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="card-body d-grid">
					<button type="button" class="next btn btn-lg btn-dark">
						<spring:message code="regist.button" />
					</button>
				</div>
			</fieldset>
			<!-- 이용 약관 동의 -->
			
			<!-- 현장등록 -->
			<fieldset>
				<div class="card-body text-center bg-primary text-white">
					<h5 class="card-title fw-bold">
						현장등록
					</h5>
				</div>
				<!-- 기본정보 -->
				<div class="card-body">
					<p class="card-title text-danger fw-bold">기본정보</p>
					<hr>
					
					<!-- 성명 -->
					<div class="row mb-3">
						<label for="FirstName" class="col-sm-2 col-form-label required">성명</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="FirstName" name="FirstName" maxlength="64" value="${param.name }" placeholder="" readonly>
						</div>
					</div>
					<!-- 성명 -->
					
					<!-- 성명 (영문) -->
					<c:if test="${infoVO.Name2_use eq 'Y' }">
					<div class="row mb-3">
						<label for="MiddleName" class="col-sm-2 col-form-label ${infoVO.Name2_req eq 'Y' ? 'required' : '' }">성명 (영문)</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="MiddleName" name="MiddleName" ${infoVO.Name2_req eq 'Y' ? 'required' : '' } maxlength="64" value="" placeholder="">
						</div>
					</div>
					</c:if>
					<!-- 성명 (영문) -->
					
					<!-- 생년월일 -->
					<c:if test="${infoVO.Birthday_use eq 'Y' }">
					<div class="row mb-3">
						<label for="LastName" class="col-sm-2 col-form-label ${infoVO.Birthday_req eq 'Y' ? 'required' : '' }">생년월일</label>
						<div class="col-sm-10">
							<div class="input-group">
								<span class="input-group-text">
									<i class="bi bi-calendar"></i>
								</span>
								<input type="text" class="form-control" data-provide="datepicker" id="LastName" name="LastName" ${infoVO.Birthday_req eq 'Y' ? 'required' : '' } value="" placeholder="">
							</div>
						</div>
					</div>
					</c:if>
					<!-- 생년월일 -->
					
					<!-- 휴대폰 -->
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label required" for="Mobile">휴대폰</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Mobile" name="Mobile" value="${param.mobile }" readonly>
						</div>
					</div>
					<!-- 휴대폰 -->
					
					<!-- 이메일 -->
					<c:if test="${infoVO.Email_use eq 'Y' }">
					<div class="row mb-3" id="email">
						<label class="col-sm-2 col-form-label ${infoVO.Email_req eq 'Y' ? 'required' : '' }" for="Email1">이메일</label>
						<div class="col-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" id="Email1" name="Email1" value="" ${infoVO.Email_req eq 'Y' ? 'required' : '' }>
								<span class="input-group-text">@</span>
								<input type="text" class="form-control" id="Email2" name="Email2" :value="selected" ${infoVO.Email_req eq 'Y' ? 'required' : '' }>
								<c:if test="${pageContext.response.locale eq 'ko' }">
								<div class="col-4">
									<select class="form-select" v-model="selected">
										<option v-for="option in options" :value="option.value">
											{{ option.text }}
										</option>
									</select>
								</div>
								</c:if>
							</div>
						</div>
					</div>
					</c:if>
					<!-- 이메일 -->
					
					<!-- 주소 -->
					<c:if test="${infoVO.Addr_use eq 'Y' }">
					<input type="hidden" name="Zip" id="Zip">
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label ${infoVO.Addr_req eq 'Y' ? 'required' : '' }" for="Addr1">주소</label>
						<div class="col-sm-10">
							<div class="input-group">
								<input type="text" class="form-control" id="Addr1" name="Addr1" value="" maxlength="100" ${infoVO.Addr_req eq "Y" ? "required" : "" } readonly placeholder="주소">
								<button class="btn btn-outline-secondary" data-bs-toggle="modal" data-bs-target="#addrModal" id="addr" type="button">주소 검색</button>
							</div>
						</div>
					</div>
					<c:if test="${infoVO.Addr2_use eq 'Y' }">
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label ${infoVO.Addr_req eq 'Y' ? 'required' : '' }" for="Addr2">상세주소</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Addr2" name="Addr2" value="" maxlength="64" ${infoVO.Addr_req eq "Y" ? "required" : "" } placeholder="상세주소를 입력하여 주세요.">
						</div>
					</div>
					</c:if>
					</c:if>
					<!-- 주소 -->
					
					<!-- 전화 -->
					<c:if test="${infoVO.Phone_use eq 'Y' }">
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label ${infoVO.Phone_req eq 'Y' ? 'required' : '' }" for="Phone">전화</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Phone" name="Phone" value="" maxlength="14">
						</div>
					</div>
					</c:if>
					<!-- 전화 -->
					
					<!-- 팩스 -->
					<c:if test="${infoVO.Fax_use eq 'Y' }">
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label ${infoVO.Fax_req eq 'Y' ? 'required' : '' }" for="Fax">팩스</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Fax" name="Fax" value="" maxlength="14">
						</div>
					</div>
					</c:if>
					<!-- 팩스 -->
					
					<!-- 홈페이지 -->
					<c:if test="${infoVO.Homepage_use eq 'Y' }">
					<div class="row mb-3">
						<label class="col-sm-2 col-form-label ${infoVO.Homepage_req eq 'Y' ? 'required' : '' }" for="Homepage">홈페이지</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="Homepage" name="Homepage" value="" maxlength="64">
						</div>
					</div>
					</c:if>
					<!-- 홈페이지 -->
					<hr>
				</div>
				<!-- 기본정보 -->
				
				<!-- 회사정보 -->
				<c:if test="${infoVO.Organization_use eq 'Y' || infoVO.Organization2_use eq 'Y' || infoVO.Department_use eq 'Y' || infoVO.Position_use eq 'Y' }">
				<div class="card-body">
					<p class="card-title fw-bold">회사 정보</p>
					<c:if test="${infoVO.Organization_req eq 'S' }">
					<div class="form-check">
						<input class="form-check-input" type="checkbox" value="N" id="Orgn1_ck" name="Orgn1_ck" onchange="$('#Orgn').toggle(!this.checked);">
						<label class="form-check-label" for="Orgn1_ck">(없음) 소속이 없을 경우 체크 하여 주시기 바랍니다.</label>
					</div>
					</c:if>
					<hr>
					
					<div id="Orgn">
						<!-- 회사명 -->
						<c:if test="${infoVO.Organization_use eq 'Y' }">
						<div class="row mb-3">
							<label class="col-sm-2 col-form-label ${infoVO.Organization_req eq 'Y' ? 'required' : '' }" for="Orgn1">회사명</label>
							<div class="col-sm-10">
								<input class="form-control" type="text" id="Orgn1" name="Orgn1" maxlength="64" ${infoVO.Organization_req eq 'Y' ? 'required' : '' }>
							</div>
						</div>
						</c:if>
						<!-- 회사명 -->
						
						<!-- 회사명 (영문) -->
						<c:if test="${infoVO.Organization2_use eq 'Y' }">
						<div class="row mb-3">
							<label class="col-sm-2 col-form-label ${infoVO.Organization2_req eq 'Y' ? 'required' : '' }" for="Orgn2">회사명 (영문)</label>
							<div class="col-sm-10">
								<input class="form-control" type="text" id="Orgn2" name="Orgn2" maxlength="64" ${infoVO.Organization2_req eq 'Y' ? 'required' : '' }>
							</div>
						</div>
						</c:if>
						<!-- 회사명 (영문) -->
						
						<!-- 부서 -->
						<c:if test="${infoVO.Department_use eq 'Y' }">
						<div class="row mb-3">
							<label class="col-sm-2 col-form-label ${infoVO.Department_req eq 'Y' ? 'required' : '' }" for="Part">부서</label>
							<div class="col-sm-10">
								<input class="form-control" type="text" id="Part" name="Part" maxlength="64" ${infoVO.Department_req eq 'Y' ? 'required' : '' }>
							</div>
						</div>
						</c:if>
						<!-- 부서 -->
						
						<!-- 직급 -->
						<c:if test="${infoVO.Position_use eq 'Y' }">
						<div class="row mb-3">
							<label class="col-sm-2 col-form-label ${infoVO.Position_req eq 'Y' ? 'required' : '' }" for="Part">직급</label>
							<div class="col-sm-10">
								<input class="form-control" type="text" id="Rank" name="Rank" maxlength="64" ${infoVO.Position_req eq 'Y' ? 'required' : '' }>
							</div>
						</div>
						</c:if>
						<!-- 직급 -->
					</div>
					<hr>
				</div>
				</c:if>
				<!-- 회사정보 -->
				
				<!-- 설문항목 -->
				<div id="surv">
					<p v-for="surv in capitalize(survs)">{{ surv }}</p>
				</div>
				<!-- 설문항목 -->
				
				<div class="card-body d-grid">
					<button type="submit" class="confirm btn btn-lg btn-dark">
						<spring:message code="regist.button" />
					</button>
				</div>
			</fieldset>
			<!-- 현장등록 -->
			
			<div class="card-footer text-body-secondary text-center">
				Copyright ⓒ SYSFORU INC. All Rights Reserved
			</div>
		</div>
	</form>
</div>

<!-- addr modal -->
<div class="modal fade" id="addrModal" tabindex="-1" aria-labelledby="addrModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-fullscreen">
		<div class="modal-content" id="addrContent">
			<div class="modal-header">
				<h1 class="modal-title fs-5" id="addrModalLabel">주소 검색</h1>
				<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
			</div>
		</div>
	</div>
</div>
<!-- addr modal -->

<script>
$(function() {
	let lang = "${pageContext.response.locale }";
	
	$("#agreeA").on("click", function() {
		$("input[type=checkbox]").prop("checked", this.checked);
	});
	
	$(".next").on("click", function() {
		if (!$("#agree").is(":checked")) {
			alert('<spring:message code="regist.agree.alert" />');
			$("#agree").focus();
			return false;
		}
		
		if (!$("#agree2").is(":checked")) {
			alert('<spring:message code="regist.collection.alert" />');
			$("#agree2").focus();
			return false;
		}
		
		nextStep(this);
	});
	
	$(".confirm").on("click", function() {
		console.log($("#registFrm").serializeArray());
	});
	
	function nextStep(e) {
		let current_fs, next_fs;
		
		current_fs = $(e).parent().parent();
		next_fs = $(e).parent().parent().next();
		
		next_fs.show();
		
		current_fs.css({
			"display" : "none"
		});
	}
	
	$("#LastName").datepicker({
		format: "yyyy-mm-dd",
		autoclose: true,
		clearBtn: false,
		disableTouchKeyboard: true,
		templates: {
			leftArrow: "&laquo;",
			rightArrow: "&raquo;"
		},
		showWeekDays: true,
		todayHighlight: true,
		language: lang
	});
	
	$("#Mobile").val((index, value) => {
		let mbs = value;
		
		if (lang === "ko") {
			let hyphen = "-";
			mbs = mbs.replace(hyphen, "");
				
			if (mbs.length <= 10) {
				mbs = mbs.slice(0, 3) + hyphen + mbs.slice(3, 6) + hyphen + mbs.slice(6, mbs.length);
			} else if (mbs.length >= 11) {
				mbs = mbs.slice(0, 3) + hyphen + mbs.slice(3, 7) + hyphen + mbs.slice(7, mbs.length);
			}
			
		}
		
		return mbs;
	});
	
	new Vue({
		el: "#email",
		data: {
			selected: "",
			options: [
				{text: "직접입력", value: ""},
				{text: "naver.com", value: "naver.com"},
				{text: "daum.net", value: "daum.net"},
				{text: "hotmail.com", value: "hotmail.com"},
				{text: "nate.com", value: "nate.com"},
				{text: "yahoo.co.kr", value: "yahoo.co.kr"},
				{text: "paran.com", value: "paran.com"},
				{text: "empas.com", value: "empas.com"},
				{text: "dreamwiz.com", value: "dreamwiz.com"},
				{text: "freechal.com", value: "freechal.com"},
				{text: "lycos.co.kr", value: "lycos.co.kr"},
				{text: "korea.com", value: "korea.com"},
				{text: "gmail.com", value: "gmail.com"},
				{text: "hanmir.com", value: "hanmir.com"},
			]
		}
	});
	
	new Vue({
		el: "#surv",
		data: {
			survs: JSON.parse(JSON.stringify(${survFoot }))
		},
		methods: {
			capitalize: function(value) {
				let filterKey = [];
				let resultJson = {};
				if (!value) return "";
				
				for (const key in value) {
					if (key.indexOf("_Use") > -1) {
						if (value[key.match("_Use").input] === "Y") {
							filterKey.push(key.substring(0, key.indexOf("_")));
						}
					}
				}
				
				filterKey.forEach(filter => {
					for (const key in value) {
						if (key.indexOf(filter) > -1) {
							if (key.indexOf("_Data") === -1) {
								resultJson[key] = value[key];
							}
						}
					}
				});
				
				return resultJson;
			}
		}
	});
	
	$("#addr").on("click", function() {
		new daum.Postcode({
			oncomplete: function(data) {
				let fullAddr = data.address;
				let extraAddr = "";
				
				if (data.addressType === "R") {
					// 법정동명이 있을 경우 추가
					if (data.bname)	{
						extraAddr += data.bname;
					}
					
					// 건물명이 있을 경우 추가
					if (data.buildingName) {
						extraAddr += (extraAddr !== "" ? ", " + data.buildingName : data.buildingName);
					}
					
					// 조합형주소의 유무에 따라 양쪽에 괄화를 추가하여 최종 주소를 만든다.
					fullAddr += (extraAddr !== "" ? " (".concat(extraAddr).concat(")") : "");
				}
				
				$("#Zip").val(data.zonecode);
				$("#Addr1").val(fullAddr);
			},
			onclose: function(state) {
				if (state === "COMPLETE_CLOSE") {
					$("#addrModal").modal("hide");
				}
				
				// 주소 모달이 닫히고 상세주소가 있으면 모달 종료 이벤트가 끝난 후
				// 상세주소로 포커스를 옮긴다.
				let addrModal = document.getElementById("addrModal");
				addrModal.addEventListener("hidden.bs.modal", event => {
					if (event.returnValue) {
						if (${infoVO.Addr2_use eq "Y" }) $("#Addr2").focus();
					}
				});
			},
			width: "100%",
			height: "100%"
		}).embed(document.getElementById("addrContent"));
	});
});
</script>