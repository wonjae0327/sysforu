<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<hr>

	<form name="frm" id="frm" method="post" action="<c:url value="/dev/ndata_ok" />" onsubmit="return false;">
		<input type="hidden" name="INCD" value="asdf8515">
	</form>
	
	<div class="row">
		<div class="col">
			<p class="lead">설문컬럼</p>
			<table class="table table-hover">
				<thead>
					<tr>
						<th>[ColName]</th>
						<th>[Ndata 설정]</th>
						<th>[현장등록 설정]</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="item" items="${colT }">
						<tr>
							<td>${item.ColName }</td>
							<td>${item.ColDisp }</td>
							<td>
								<c:set var="survNum" value="${item.ColName }_Use" />
								<c:choose>
									<c:when test="${surv[survNum] eq 'Y' }">
										<c:set var="title" value="${item.ColName }_Title" />
										${surv[title] }
									</c:when>
									<c:otherwise>
										None
									</c:otherwise>
								</c:choose>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
	
	<div id="app">
		<div class="row">
			<div class="col">
				<p class="lead">Ndata-설문코드</p>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>[SurveyN]</th>
							<th>[SurveyC]</th>
							<th>[SurveyS]</th>
							<th>[SurveyD]</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="survt" items="${survT }">
							<tr>
								<td>${survt.SurveyN }</td>
								<td>${survt.SurveyC }</td>
								<td>${survt.SurveyS }</td>
								<td>${survt.SurveyD }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col">
				<p class="lead">설문코드-적용</p>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>[SurveyN]</th>
							<th>[SurveyC]</th>
							<th>[SurveyS]</th>
							<th>[SurveyD]</th>
						</tr>
					</thead>
					<tbody>
						<template v-for="(values, key, index) in code">
							<template v-for="(v, k, i) in values">
								<tr v-if="k.indexOf('Surv') > -1">
									<td>{{ k.replace("Surv", "") }}</td>
									<td>{{ v[i].Data }}</td>
									<td>{{ v[++i].Data }}</td>
									<td></td>
								</tr>
							</template>
						</template>
					</tbody>
				</table>
			</div>
		</div>
		<button type="button" class="btn btn-primary" @click="ndataSave">적용</button>
	</div>
	
<hr>
<script>
	var code = JSON.parse(JSON.stringify(${survCode }));
	var foot = ${survFoot };
	
	new Vue({
		el: "#app",
		data: {
			code: code
		},
		methods: {
			ndataSave() {
				axios.post('<c:url value="/dev/ndata_ok" />', {
					"foot": JSON.stringify(foot),
					"code": JSON.stringify(code)
				}, {
					headers: {
						"Content-Type": "application/json; charset=UTF-8",
					}
				})
				.then(result => {
					if (result.data) {
						alert("ok");
						location.href = '<c:url value="/dev/ndata" />';
					} else {
						alert("설정 확인 후 다시 시도하시기 바랍니다.");
					}
				})
				.catch(error => {
					console.log(error);
				});
			} // .end ndataSave
		}
	});
</script>