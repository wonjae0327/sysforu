package com.sysforu.demo.vo;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InitVO {

	@JsonIgnore
	private transient MultipartFile[] LOGO_UP;
	@JsonProperty("LOGO_FILE")
	private String LOGO_FILE = "";
	@JsonProperty("ExhibitionNo")
	private String ExhibitionNo = "";
	@JsonProperty("DBName")
	private String DBName = "";
	@JsonProperty("titleStr")
	private String titleStr = "";
	@JsonProperty("organizerName")
	private String organizerName = "";
	@JsonProperty("exhibitionSiteUrl")
	private String exhibitionSiteUrl = "";
	@JsonProperty("IN_FONT")
	private String IN_FONT = "";
	@JsonProperty("IN_DN")
	private String IN_DN = "";
	@JsonProperty("IN_LANG")
	private String IN_LANG = "";
	@JsonProperty("IN_REG")
	private String IN_REG = "";
	@JsonProperty("IN_IV")
	private String IN_IV = "";
	@JsonProperty("IN_C19")
	private String IN_C19 = "";
	@JsonProperty("esys")
	private String esys = "";
	@JsonProperty("IN_SDHEAD")
	private String IN_SDHEAD = "";
	
}
