package com.sysforu.demo.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.sysforu.demo.service.CommonService;
import com.sysforu.demo.vo.InfoVO;
import com.sysforu.demo.vo.InitVO;

@Controller
@RequestMapping(value = "/dev")
public class DevController {
	@Autowired
	CommonService commonService;

	@GetMapping("/init")
	public String init(HttpServletRequest request, Model model) throws Exception {
		model.addAttribute("init", request.getAttribute("initVO"));		
		return "dev/init";
	}
	
	@PostMapping("/init_ok")
	@ResponseBody
	public Map<String, Object> initOkAjax(InitVO init) {
		return commonService.setInit(init);
	}
	
	@GetMapping("/info")
	public String info(HttpServletRequest request, Model model) {
		model.addAttribute("init", request.getAttribute("initVO"));
		model.addAttribute("info", request.getAttribute("infoVO"));
		return "dev/info";
	}
	
	@PostMapping("/info_ok")
	@ResponseBody
	public Map<String, Object> infoOkAjax(InfoVO info) {		
		return commonService.setInfo(info);
	}
	
	@GetMapping("/surv")
	public String surv(HttpServletRequest request, Model model) {
		model.addAttribute("init", request.getAttribute("initVO"));
		model.addAttribute("surv", request.getAttribute("surv"));
		return "dev/surv";
	}
	
	@PostMapping("/surv_ok")
	@ResponseBody
	public Map<String, Object> survOkAjax(HttpServletRequest request) {
		return commonService.setSurv(request);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/gubun")
	public String gubun(HttpServletRequest request, Model model) {
		Map<String, Object> initMap = (Map<String, Object>) request.getAttribute("initVO");
		model.addAttribute("init", initMap);
		model.addAttribute("surv", request.getAttribute("surv"));
		model.addAttribute("gubun", commonService.selectExhibitT(initMap));
		model.addAttribute("gubunBody", request.getAttribute("gubunBody"));
		return "dev/gubun";
	}
	
	@PostMapping("/gubun_ok")
	@ResponseBody
	public Map<String, Object> gubunOkAjax(HttpServletRequest request) {
		return commonService.setGubun(request);
	}
	
	@SuppressWarnings("unchecked")
	@GetMapping("/ndata")
	public String ndata(HttpServletRequest request, Model model) {
		Map<String, Object> initMap = (Map<String, Object>) request.getAttribute("initVO");
		String survJson = new Gson().toJson(request.getAttribute("surv"));
		model.addAttribute("init", initMap);
		model.addAttribute("surv", request.getAttribute("surv"));
		model.addAttribute("survFoot", survJson);
		model.addAttribute("colT", request.getAttribute("colT"));
		model.addAttribute("survCode", request.getAttribute("survCode"));
		model.addAttribute("gubun", commonService.selectExhibitT(initMap));
		model.addAttribute("gubunBody", request.getAttribute("gubunBody"));
		
		return "dev/ndata";
	}
	
	@PostMapping("/ndata_ok")
	@ResponseBody
	public Map<String, Object> ndataOkAjax(@RequestBody Map<String, Object> entity) {
		//TODO: process POST request
		return commonService.setNdata(entity);
	}
	
	@PostMapping("/duplicateCheck")
	@ResponseBody
	public Map<String, Object> duplicateCheck(@RequestBody Map<String, Object> entity) {
		//TODO: process POST request
		return commonService.duplicateCheck(entity);
	}
	
}
