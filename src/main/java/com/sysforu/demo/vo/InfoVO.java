package com.sysforu.demo.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class InfoVO {

	@JsonProperty("Name2_use")
	private String Name2_use = "";
	@JsonProperty("Name2_req")
	private String Name2_req = "";
	@JsonProperty("Name2_use_N")
	private List<String> Name2_use_N = null;
	@JsonProperty("Birthday_use")
	private String Birthday_use = "";
	@JsonProperty("Birthday_req")
	private String Birthday_req = "";
	@JsonProperty("Birthday_use_N")
	private List<String> Birthday_use_N = null;
	@JsonProperty("Organization_use")
	private String Organization_use = "";
	@JsonProperty("Organization_req")
	private String Organization_req = "";
	@JsonProperty("Organization_use_N")
	private List<String> Organization_use_N = null;
	@JsonProperty("Organization2_use")
	private String Organization2_use = "";
	@JsonProperty("Organization2_req")
	private String Organization2_req = "";
	@JsonProperty("Organization2_use_N")
	private List<String> Organization2_use_N = null;
	@JsonProperty("Department_use")
	private String Department_use = "";
	@JsonProperty("Department_req")
	private String Department_req = "";
	@JsonProperty("Department_use_N")
	private List<String> Department_use_N = null;
	@JsonProperty("Position_use")
	private String Position_use = "";
	@JsonProperty("Position_req")
	private String Position_req = "";
	@JsonProperty("Position_use_N")
	private List<String> Position_use_N = null;
	@JsonProperty("Addr_use")
	private String Addr_use = "";
	@JsonProperty("Addr_req")
	private String Addr_req = "";
	@JsonProperty("Addr2_use")
	private String Addr2_use = "";
	@JsonProperty("Addr_put")
	private String Addr_put = "";
	@JsonProperty("Addr_use_N")
	private List<String> Addr_use_N = null;
	@JsonProperty("Email_use")
	private String Email_use = "";
	@JsonProperty("Email_req")
	private String Email_req = "";
	@JsonProperty("Email_use_N")
	private List<String> Email_use_N = null;
	@JsonProperty("Phone_use")
	private String Phone_use = "";
	@JsonProperty("Phone_req")
	private String Phone_req = "";
	@JsonProperty("Phone_use_N")
	private List<String> Phone_use_N = null;
	@JsonProperty("Fax_use")
	private String Fax_use = "";
	@JsonProperty("Fax_req")
	private String Fax_req = "";
	@JsonProperty("Fax_use_N")
	private List<String> Fax_use_N = null;
	@JsonProperty("Homepage_use")
	private String Homepage_use = "";
	@JsonProperty("Homepage_req")
	private String Homepage_req = "";
	@JsonProperty("Homepage_use_N")
	private List<String> Homepage_use_N = null;
	
}
