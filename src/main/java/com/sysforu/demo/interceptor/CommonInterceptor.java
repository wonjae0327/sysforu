package com.sysforu.demo.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.sysforu.demo.service.CommonService;

public class CommonInterceptor extends HandlerInterceptorAdapter {
	@Autowired
	CommonService commonService;

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub
		super.postHandle(request, response, handler, modelAndView);
	}
	
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		request.setAttribute("initVO", commonService.getInit());
		request.setAttribute("infoVO", commonService.getInfo());
		request.setAttribute("surv", commonService.getSurv());
		request.setAttribute("colT", commonService.selectColumnT());
		request.setAttribute("survT", commonService.selectSurvT());
		request.setAttribute("survCode", commonService.getSurvCode());
		request.setAttribute("gubunBody", commonService.getGubun());
		return super.preHandle(request, response, handler);
	}
}
