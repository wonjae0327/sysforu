package com.sysforu.demo.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CommonMapper {

	Map<String, Object> selectExhibitT(Map<String, Object> initMap);
	List<Map<String, Object>> selectColumnT();
	List<Map<String, Object>> selectSurvT();
	int insertSurveyT(Map<String, Object> param);
	int deleteSurveyT(String surveyN);
	Integer duplicateCheck(Map<String, Object> entity);
}
