<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div data-bs-theme="dark" class="sidebar border border-right col-md-3 col-lg-2 p-0 bg-body-tertiary">
	<div class="offcanvas-md offcanvas-end bg-body-tertiary" tabindex="-1" id="sidebarMenu" aria-labelledby="sidebarMenuLabel">
		<div class="offcanvas-header">
			<h5 class="offcanvas-title" id="sidebarMenuLabel">sysforu</h5>
			<button type="button" class="btn-close" data-bs-dismiss="offcanvas" data-bs-target="#sidbarMenu" aria-label="Close"></button>
		</div>
		<div class="offcanvas-body d-md-flex flex-column p-0 pt-lg-3 overflow-y-auto">
			<ul class="nav flex-column">
				<!--<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" aria-current="page" href="#">메인</a>
				</li>-->
				<!-- <li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="#">비밀번호 설정</a>
				</li>-->
				<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="<c:url value='/dev/init' />">기초/기능 설정</a>
				</li>
				<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="<c:url value='/dev/info' />">기본 수집 설정</a>
				</li>
				<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="<c:url value='/dev/surv' />">설문설정 1~16</a>
				</li>
				<!--<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="#">설문코드 일괄설정</a>
				</li>-->
				<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="<c:url value='/dev/gubun' />">구분번호/바코드색상 설정</a>
				</li>
				<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="<c:url value='/dev/ndata' />">ndata 설문 확인/적용</a>
				</li>
				<!--<li class="nav-item">
					<a class="nav-link d-flex align-items-center gap-2 text-white" href="#">esys 관리자 설정</a>
				</li>-->
			</ul>
		</div>
	</div>
</div>