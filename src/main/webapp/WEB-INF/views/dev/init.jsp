<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>${init.titleStr }</title>
</head>
<body>
	
	<hr>
	
	<div style="max-width: 600px; margin: 0 auto;">
		<div class="col-sl-12">
			<div class="panel panel-info">
				<div class="panel-body">
					
					<!-- <a href="#">[메인]</a>
					<a href="#">[비밀번호 설정]</a>
					<a href="<c:url value='/dev/init' />">[기초/기능 설정]</a>
					<a href="<c:url value='/dev/info' />">[기본 수집 설정]</a>
					<a href="#">[설문설정 1~16]</a>
					<a href="#">[설문코드 일괄설정]</a>
					<a href="#">[구분번호/바코드색상 설정]</a>
					<a href="#">[esys 관리자 설정]</a>-->
					
					<form name="frm" id="frm" method="post" action="<c:url value='/dev/init_ok' />" onsubmit="return false;" ENCTYPE="multipart/form-data">
						<input type="hidden" name="INCD" value="asdf8515">
						
						<!-- 기초설정  -->
						<p class="lead fw-bold my-3">기초설정</p>
						
						<div class="row mb-3">
							<label for="LOGO_UP" class="col-sm-4 col-form-label fw-bold">상단이미지</label>
							<div class="col-sm-8">
								<c:if test="${not empty init.LOGO_FILE }">
									<img src="<c:url value='/upload/images/${init.LOGO_FILE }' />" class="img-fluid" alt="logo">
								</c:if>
								<input type="hidden" name="LOGO_FILE" value="${init.LOGO_FILE }">
								<input type="file" class="form-control" name="LOGO_UP" id="LOGO_UP">
								<a href="https://developers.kakao.com/tool/clear/og" target="_blank">[카카오 이미지 삭제]</a>
							</div>
						</div>
						
						<div class="row mb-3 fw-bold">
							<div class="col-sm-4">
								구분
							</div>
							<div class="col-sm-4">
								설정
							</div>
							<div class="col-sm-4">
								ndata
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="ExhibitionNo" class="col-sm-4 col-form-labael fw-bold">전시코드</label>
							<div class="col-sm-4">
								<input type="text" class="form-control" name="ExhibitionNo" id="ExhibitionNo" value="${init.ExhibitionNo }" maxlength="3" style="width: 100%;">
								<span class="text-danger">방번호 3자리</span>
							</div>
							<div class="col-sm-4">
								${ncode }
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="DBName" class="col-sm-4 fw-bold">DB</label>
							<div class="col-sm-4">
								<input type="text" name="DBName" id="DBName" class="form-control" value="${init.DBName }" maxlength="10" style="width: 100%;">
								<span class="text-danger">데이터베이스명</span>
							</div>
							<div class="col-sm-4">
								${ndb }
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="titleStr" class="col-sm-4 fw-bold">전시명</label>
							<div class="col-sm-4">
								<input type="text" name="titleStr" id="titleStr" class="form-control" value="${init.titleStr }" maxlength="10" style="width: 100%;">
							</div>
							<div class="col-sm-4">
								${nexh }
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="organizerName" class="col-sm-4 fw-bold">주최자</label>
							<div class="col-sm-4">
								<input type="text" name="organizerName" id="organizerName" class="form-control" value="${init.organizerName }" maxlength="10" style="width: 100%;">
							</div>
							<div class="col-sm-4">
								${ncom }
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="exhibitionSiteUrl" class="col-sm-4 fw-bold">전시 홈페이지</label>
							<div class="col-sm-4">
								<input type="text" name="exhibitionSiteUrl" id="exhibitionSiteUrl" class="form-control" value="${init.exhibitionSiteUrl }" maxlength="200" style="width: 100%;">
							</div>
							<div class="col-sm-4">
								-
							</div>
						</div>
						<!-- 기초설정  -->
						
						<!-- 기능설정 -->
						<p class="lead fw-bold my-3">기능설정</p>
						
						<div class="row mb-3">
							<label for="IN_FONT" class="col-sm-4 col-form-labael fw-bold">폰트크기</label>
							<div class="col-sm-8">
								<select class="form-select" name="IN_FONT" id="IN_FONT">
									<option value="1" ${init.IN_FONT eq "1" ? "selected" : null }>기본</option>
									<option value="2" ${init.IN_FONT eq "2" ? "selected" : null }>크게</option>
									<option value="3" ${init.IN_FONT eq "3" ? "selected" : null }>아주크게</option>
								</select>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_DN" class="col-sm-4 col-form-labael fw-bold">이전버전용</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="IN_DN" id="IN_DN" value="Y" ${init.IN_DN eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="IN_DN">
									사용
								</label>
								<c:choose>
									<c:when test="${init.IN_DN eq 'Y' }">
										<br>
										<span class="text-danger">
											Surv1:결제구분,
											Extd4:초청장번호,
											Extd5:휴대폰동의,
											Extd6:이메일동의
										</span>
									</c:when>
									<c:otherwise>
										<br>
										<span class="text-danger">
											sintp:결제구분,
											RND:초청장번호,
											MBY:휴대폰동의,
											EMY:이메일동의
										</span>
									</c:otherwise>
								</c:choose>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_LANG" class="col-sm-4 col-form-labael fw-bold">영문사용</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="IN_LANG" id="IN_LANG" value="Y" ${init.IN_LANG eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="IN_LANG">
									사용
								</label>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_REG" class="col-sm-4 col-form-labael fw-bold">입장버튼</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="IN_REG" id="IN_REG" value="Y" ${init.IN_REG eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="IN_REG">
									사용
								</label>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_IV" class="col-sm-4 col-form-labael fw-bold">초청장등록버튼</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="IN_IV" id="IN_IV" value="Y" ${init.IN_IV eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="IN_IV">
									사용
								</label>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_C19" class="col-sm-4 col-form-labael fw-bold">코로나설문</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="IN_C19" id="IN_C19" value="Y" ${init.IN_C19 eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="IN_C19">
									사용
								</label>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="esys" class="col-sm-4 col-form-labael fw-bold">관리자설정(esys)</label>
							<div class="col-sm-8">
								<input class="form-check-input" type="checkbox" name="esys" id="esys" value="Y" ${init.esys eq "Y" ? "checked" : null }>
								<label class="form-check-label" for="esys">
									사용
								</label>
							</div>
						</div>
						
						<div class="row mb-3">
							<label for="IN_SDHEAD" class="col-sm-4 col-form-labael fw-bold">센드포유 체크코드</label>
							<div class="col-sm-8">
								<input type="text" class="form-control" name="IN_SDHEAD" id="IN_SDHEAD" value="${init.IN_SDHEAD }" maxlength="8" ${init.IN_SDHEAD eq "Y" ? "checked" : null }>
								<span class="text-danger">ex)0001-001</span>
							</div>
						</div>
						<!-- 기능설정 -->
						
						<!-- <button type="button" class="btn btn-primary" onclick="document.frm.submit();">저장</button> -->
						<button type="button" class="btn btn-primary" onclick="initSave();">저장</button>
					</form>
				</div>			
			</div>
		</div>
	</div>
	
	<hr>
	
	<script type="text/javascript" src="/webjars/jquery/3.7.1/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/webjars/bootstrap/5.3.2/dist/js/bootstrap.min.js"></script>
	<script type="text/javascript">
		function initSave() {
			var formData = new FormData(document.getElementById('frm'));
			
			$.ajax({
				type : 'post',
				enctype : 'multipart/form-data',
				url : '<c:url value="/dev/init_ok" />',
				data : formData,
				processData : false,
				contentType : false,
				cache : false,
				success : function(data) {
					if (data.success === "true") {
						alert("ok");
						//location.reload();
						location.href = '<c:url value="/dev/init" />';
					} else {
						alert("저장에 실패하였습니다.\n확인 후 다시 시도하시기 바랍니다.");
					}
				},
				error : function(request, status, error) {
					console.log(error);
				}
			});
		}
	</script>
</body>
</html>