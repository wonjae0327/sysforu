<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<hr>

	<form name="frm" id="frm" method="post" action="<c:url value='/dev/surv_ok' />" onsubmit="return false;">
		<input type="hidden" name="INCD" value="asdf8515">
		
		<p class="lead fw-bold my-3">설문설정 1~16</p>
		
		<a href="<c:url value='/upload/files/설문입력.xlsx' />">[설정용 엑셀파일 다운로드]</a>
		
		<c:forEach var="i" begin="${init.IN_DN ne 'Y' ? 1 : 2 }" end="16">
			<c:set var="surv_use" value="Surv${i }_Use" />
			<c:set var="surv_use_n" value="Surv${i }_Use_N" />
			<c:set var="surv_title" value="Surv${i }_Title" />
			<c:set var="surv_title_en" value="Surv${i }_Title_en" />
			<c:set var="surv_type" value="Surv${i }_Type" />
			<c:set var="surv_required" value="Surv${i }_Required" />
			<c:set var="surv_data" value="Surv${i }_Data" />
			
			<h5 class="my-3">설문${i }<input class="${surv[surv_use] eq 'Y' ? '' : 'collapsed' }" type="checkbox" name="Surv${i }_Use" id="Surv${i }_Use" value="Y" data-bs-toggle="collapse" data-bs-target="#Surv${i }Collapse" aria-expanded="${surv[surv_use] eq 'Y' ? true : false }" aria-controls="Surv${i }Collapse" ${surv[surv_use] eq 'Y' ? 'checked' : '' }></h5>
			
			<div class="collapse ${surv[surv_use] eq 'Y' ? 'show' : '' }" id="Surv${i }Collapse">
				<div class="card">
					<div class="card-body">
						<!-- 등록구분별 미사용 체크 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label for="Surv${i }" class="fw-bold">등록구분별 미사용 체크</label>
							</div>
							<div class="col" id="Surv${i }">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="Surv${i }_Use_N" id="Surv${i }_Use_N_in" value="in" ${surv[surv_use_n][0] eq 'in' ? 'checked' : '' }>
									<label class="form-check-label" for="Surv${i }_Use_N_in">in</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="Surv${i }_Use_N" id="Surv${i }_Use_N_bf" value="bf" ${surv[surv_use_n][1] eq 'bf' ? 'checked' : '' }>
									<label class="form-check-label" for="Surv${i }_Use_N_bf">bf</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="Surv${i }_Use_N" id="Surv${i }_Use_N_iv" value="iv" ${surv[surv_use_n][2] eq 'iv' ? 'checked' : '' }>
									<label class="form-check-label" for="Surv${i }_Use_N_iv">iv</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="Surv${i }_Use_N" id="Surv${i }_Use_N_sd" value="sd" ${surv[surv_use_n][3] eq 'sd' ? 'checked' : '' }>
									<label class="form-check-label" for="Surv${i }_Use_N_sd">sd</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" name="Surv${i }_Use_N" id="Surv${i }_Use_N_co" value="co" ${surv[surv_use_n][4] eq 'co' ? 'checked' : '' }>
									<label class="form-check-label" for="Surv${i }_Use_N_co">co</label>
								</div>
							</div>
						</div>
						<!-- 등록구분별 미사용 체크 -->
						
						<!-- 타이틀 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="Surv${i }_Title">타이틀</label>
							</div>
							<div class="col">
								<input type="text" name="Surv${i }_Title" id="Surv${i }_Title" class="form-control" value="${surv[surv_title] }" maxlength="200">
							</div>
						</div>
						<!-- 타이틀 -->
						
						<!-- 타이틀 (영문) -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="Surv${i }_Title_en">타이틀 (영문)</label>
							</div>
							<div class="col">
								<input type="text" name="Surv${i }_Title_en" id="Surv${i }_Title_en" class="form-control" value="${surv[surv_title_en] }" maxlength="200">
							</div>
						</div>
						<!-- 타이틀 (영문) -->
						
						<!-- 수집방식 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label for="Surv${i }_Type" class="fw-bold">수집방식</label>
							</div>				
							<div class="col">
								<select class="form-select" name="Surv${i }_Type" id="Surv${i }_Type">
									<option value="INPUTSELECT" ${surv[surv_type] eq 'INPUTSELECT' ? 'selected' : '' }>셀렉트박스(단일선택)</option>
									<option value="INPUTRADIO" ${surv[surv_type] eq 'INPUTRADIO' ? 'selected' : '' }>라디오박스(단일선택)</option>
									<option value="INPUTCHECKBOX" ${surv[surv_type] eq 'INPUTCHECKBOX' ? 'selected' : '' }>체크박스(다중선택)</option>
									<option value="INPUTTEXT" ${surv[surv_type] eq 'INPUTTEXT' ? 'selected' : '' }>텍스트 박스</option>
									<option value="INPUTCATE01" ${surv[surv_type] eq 'INPUTCATE01' ? 'selected' : '' }>Reed_관심품목(대분류)</option>
									<option value="INPUTCATE02" ${surv[surv_type] eq 'INPUTCATE02' ? 'selected' : '' }>Reed_관심품목(소분류)</option>
								</select>
							</div>				
						</div>
						<!-- 수집방식 -->
						
						<!-- 필수여부 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="Surv${i }_Required">필수여부</label>
							</div>
							<div class="col">
								<input class="form-check-input" type="checkbox" name="Surv${i }_Required" id="Surv${i }_Required" value="Y" ${surv[surv_required] eq 'Y' ? 'checked' : '' }>
							</div>
						</div>
						<!-- 필수여부 -->
						
						<!-- 설문설정 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label for="Surv${i }_Data" class="form-label fw-bold">설문설정</label>
							</div>
						</div>
						<div class="row g-3 mb-3">
							<div class="col">
								<textarea class="form-control" name="Surv${i }_Data" id="Surv${i }_Data" rows="10" aria-describedby="Surv${i }_DataHelpBlock">${surv[surv_data] }</textarea>
							</div>
							<div id="Surv${i }_DataHelpBlock" class="form-text text-danger">
								ex)<br>
								A|일반|General<br>
								B|바이어|Buyer
							</div>
						</div>
						<!-- 설문설정 -->
					</div>
				</div>
			</div>
			
		</c:forEach>
		
		<hr>
		
		<h5 class="mb-3">바코드문자 추가 설문 안내 문구</h5>
		
		<!-- 안내문구 -->
		<div class="row g-3 mb-3">
			<div class="col">
				<label class="fw-bold form-label" for="Other_Info">안내문구</label>
			</div>
		</div>
		<div class="row g-3 mb-3">
			<div class="col">
				<textarea class="form-control" name="Other_Info" id="Other_Info" rows="10"></textarea>
			</div>
		</div>
		<!-- 안내문구 -->
		
		<c:forEach var="i" begin="1" end="3">
			<c:set var="etc_use" value="ETC${i }_Use" />
			<c:set var="etc_title" value="ETC${i }_Title" />
			<c:set var="etc_colnm" value="ETC${i }_Colnm" />
			<c:set var="etc_type" value="ETC${i }_Type" />
			<c:set var="etc_data" value="ETC${i }_Data" />
		
			<h5 class="my-3">바코드문자 추가 설문${i }<input class="${surv[etc_use] eq 'Y' ? '' : 'collapsed' }" type="checkbox" name="ETC${i }_Use" value="Y" data-bs-toggle="collapse" data-bs-target="#ETC${i }Collapse" aria-expanded="${surv[etc_use] eq 'Y' ? true : false }" aria-controls="ETC${i }Collapse" ${surv[etc_use] eq 'Y' ? 'checked' : '' }></h5>
			
			<div class="collapse ${surv[etc_use] eq 'Y' ? 'show' : '' } mb-3" id="ETC${i }Collapse">
				<div class="card">
					<div class="card-body">
						
						<!-- 바코드문자 타이틀 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="ETC${i }_Title">타이틀</label>
							</div>
							<div class="col">
								<input class="form-control" type="text" name="ETC${i }_Title" id="ETC${i }_Title" value="${surv[etc_title] }">
							</div>
						</div>
						<!-- 바코드문자 타이틀 -->
						
						<!-- 컬럼설정 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="ETC${i }_Colnm">컬럼설정</label>
							</div>
							<div class="col">
								<input class="form-control" type="text" name="ETC${i }_Colnm" id="ETC${i }_Colnm" value="${surv[etc_colnm] }" aria-describedby="ETC${i }_ColnmHelpBlock">
								<div id="ETC${i }_ColnmHelpBlock" class="form-text text-danger">
									ex)Extd2
								</div>
							</div>
						</div>
						<!-- 컬럼설정 -->
						
						<!-- 바코드문자 수집방식 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="ETC${i }_Type">수집방식</label>
							</div>
							<div class="col">
								<select class="form-select" name="ETC${i }_Type" id="ETC${i }_Type">
									<option value="INPUTSELECT" ${surv[etc_type] eq 'INPUTSELECT' ? 'selected' : '' }>셀렉트박스(단일선택)</option>
									<option value="INPUTRADIO" ${surv[etc_type] eq 'INPUTRADIO' ? 'selected' : '' }>라디오박스(단일선택)</option>
									<option value="INPUTCHECKBOX" ${surv[etc_type] eq 'INPUTCHECKBOX' ? 'selected' : '' }>체크박스(다중선택)</option>
									<option value="INPUTTEXT" ${surv[etc_type] eq 'INPUTTEXT' ? 'selected' : '' }>텍스트(컬럼자리수확인필요)</option>
									<option value="INPUTEMAIL" ${surv[etc_type] eq 'INPUTEMAIL' ? 'selected' : '' }>이메일</option>
								</select>
							</div>
						</div>
						<!-- 바코드문자 수집방식 -->
						
						<!-- 바코드문자 설문설정 -->
						<div class="row g-3 mb-3">
							<div class="col">
								<label class="fw-bold" for="ETC${i }_Data">설문설정</label>
							</div>
						</div>
						<div class="row g-3 mb-3">
							<div class="col">
								<textarea class="form-control" name="ETC${i }_Data" id="ETC${i }_Data" rows="10" aria-describedby="ETC${i }_DataHelpBlock">${surv[etc_data] }</textarea>
								<div id="ETC${i }_DataHelpBlock" class="form-text text-danger">
									ex)<br>
									A|일반<br>
									B|바이어
								</div>
							</div>
						</div>
						<!-- 바코드문자 설문설정 -->
						
					</div>
				</div>
			</div>
		</c:forEach>
		
		<!-- <button type="button" class="btn btn-primary" onclick="survSave();">저장</button> -->
		<input type="submit" class="btn btn-primary" value="저장" onclick="survSave();">
	</form>

<hr>
<script>
	$(function() {
		// 설문n 체크를 해제하면 하위 내용을 초기화
		$("input[name$=_Use]").on("click", function() {
			if (!this.checked) {
				var name = this.name;
				var clearName = name.substr(0, name.indexOf("_"));
				$("input[name^=" + clearName + "], textarea[name^=" + clearName + "], select[name^=" + clearName + "]").each(function(idx, el) {
					this.checked = false;
					this.selected = false;
					this.value = "";
				});
			}
		});
	});
	
	function survSave() {
		$.ajax({
			type: 'post',
			url: '<c:url value="/dev/surv_ok" />',
			data: $("#frm").serialize(),
			success: function(data) {
				if (data.success) {
					alert("ok");
					location.href = '<c:url value="/dev/surv" />';
				} else {
					alert("설정 확인 후 다시 시도하시기 바랍니다.");
				}
			},
			error: function(req, stat, err) {
				console.log(err);
			}
		});
	}
</script>