<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<hr>

	<form name="frm" id="frm" method="post" action="<c:url value='/dev/gubun_ok' />" onsubmit="return false;">
		<input type="hidden" name="INCD" value="asdf8515">
		
		<p class="lead fw-bold">등록 구분번호 설정</p>
		
		<div class="mb-3">
			<small class="text-danger">각 구분에 따라 등록했을 때 적용되는 구분번호, 저장 필드 설정입니다.<br>
			예) 현장등록에 구분번호 83, Surv2 - A로 설정 → 등록번호 83으로 시작, Surv2 - A값 저장.</small>
		</div>
		
		<div class="row mb-3 fw-bold text-center">
			<div class="col">
				구분
			</div>
			<div class="col">
				구분번호
			</div>
			<div class="col">
				<label for="DV_C1" class="form-label fw-bold">기본값1</label>
				<select name="DV_C1" id="DV_C1" class="form-select-label">
					<option selected>선택</option>
					<c:forEach var="i" begin="${init.IN_DN ne 'Y' ? 1 : 2 }" end="16">
						<c:set var="DV_C1val" value="Surv${i }" />
						<option value="Surv${i }" ${gubunBody.DV_C1 eq DV_C1val ? 'selected' : '' }>Surv${i }</option>
					</c:forEach>
				</select>
			</div>
			<div class="col">
				<label for="DV_C2" class="form-label fw-bold">기본값2</label>
				<select name="DV_C2" id="DV_C2" class="form-select-label">
					<option selected>선택</option>
					<c:forEach var="i" begin="${init.IN_DN ne 'Y' ? 1 : 2 }" end="16">
						<c:set var="DV_C2val" value="Surv${i }" />
						<option value="Surv${i }" ${gubunBody.DV_C2 eq DV_C2val ? 'selected' : '' }>Surv${i }</option>
					</c:forEach>
				</select>
			</div>
		</div>
		
		<!-- 현장등록 -->
		<div class="row mb-3">
			<div class="col">
				<label for="in_code" class="fw-bold">현장등록 : in</label>
			</div>
			<div class="col">
				<input type="text" name="in_code" id="in_code" class="form-control" value="${gubunBody.in_code }">
			</div>
			<div class="col">
				<input type="text" name="in_dv1" id="in_dv1" class="form-control" value="${gubunBody.in_dv1 }">
			</div>
			<div class="col">
				<input type="text" name="in_dv2" id="in_dv2" class="form-control" value="${gubunBody.in_dv2 }">
			</div>
		</div>
		<!-- 현장등록 -->
		
		<!-- 사전등록 -->
		<div class="row mb-3">
			<div class="col">
				<label for="bf_code" class="fw-bold">사전등록 : bf</label>
			</div>
			<div class="col">
				<input type="text" name="bf_code" id="bf_code" class="form-control" value="${gubunBody.bf_code }">
			</div>
			<div class="col">
				<input type="text" name="bf_dv1" id="bf_dv1" class="form-control" value="${gubunBody.bf_dv1 }">
			</div>
			<div class="col">
				<input type="text" name="bf_dv2" id="bf_dv2" class="form-control" value="${gubunBody.bf_dv2 }">
			</div>
		</div>
		<!-- 사전등록 -->
		
		<!-- 센드포유 -->
		<div class="row mb-3">
			<div class="col">
				<label for="sd_code" class="fw-bold">센드포유 : sd</label>
			</div>
			<div class="col">
				<input type="text" name="sd_code" id="sd_code" class="form-control" value="${gubunBody.sd_code }">
			</div>
			<div class="col">
				<input type="text" name="sd_dv1" id="sd_dv1" class="form-control" value="${gubunBody.sd_dv1 }">
			</div>
			<div class="col">
				<input type="text" name="sd_dv2" id="sd_dv2" class="form-control" value="${gubunBody.sd_dv2 }">
			</div>
		</div>
		<!-- 센드포유 -->
		
		<!-- 초청장 -->
		<div class="row mb-3">
			<div class="col">
				<label for="iv_code" class="fw-bold">초청장 : iv</label>
			</div>
			<div class="col">
				<input type="text" name="iv_code" id="iv_code" class="form-control" value="${gubunBody.iv_code }">
			</div>
			<div class="col">
				<input type="text" name="iv_dv1" id="iv_dv1" class="form-control" value="${gubunBody.iv_dv1 }">
			</div>
			<div class="col">
				<input type="text" name="iv_dv2" id="iv_dv2" class="form-control" value="${gubunBody.iv_dv2 }">
			</div>
		</div>
		<!-- 초청장 -->
		
		<!-- 참가업체 -->
		<div class="row mb-3">
			<div class="col">
				<label for="co_code" class="fw-bold">참가업체 : co</label>
			</div>
			<div class="col">
				<input type="text" name="co_code" id="co_code" class="form-control" value="${gubunBody.co_code }">
			</div>
			<div class="col">
				<input type="text" name="co_dv1" id="co_dv1" class="form-control" value="${gubunBody.co_dv1 }">
			</div>
			<div class="col">
				<input type="text" name="co_dv2" id="co_dv2" class="form-control" value="${gubunBody.co_dv2 }">
			</div>
		</div>
		<!-- 참가업체 -->
		
		<!-- 특정일 설정 -->
		<p class="lead">특정일 설정 (<fmt:formatDate value="${gubun.FromDate }" pattern="yyyy-MM-dd" /> ~ <fmt:formatDate value="${gubun.ToDate }" pattern="yyyy-MM-dd" />)</p>
		<div class="mb-3">
			<small>ndata에서 행사 일정은 가져오나 ndata 에서 행사일자가 변경될경우 특정일 설정을 다시 확인해 주시기 바랍니다.</small>
		</div>
		<div class="row mb-3">
			<c:choose>
				<c:when test="${not empty init.DBName }">
					<div class="col">
						<c:set var="spcdate" value="${fn:split(gubunBody.arrSPCDATE, ',') }" />
						<c:forEach var="date" items="${gubun.dateList }" varStatus="i">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" value="${date }" name="SPCDATE" id="SPCDATE${i.count }" ${spcdate[i.index] eq date ? 'checked' : '' }>
								<label class="form-check-label" for="SPCDATE${i.count }">${date }</label>
							</div>
						</c:forEach>
					</div>
				</c:when>
				<c:otherwise>
					<div class="col">
						<p>연결 디비를 설정해 주세요.
					</div>
				</c:otherwise>
			</c:choose>
		</div>
		<!-- 특정일 설정 -->
		
		<!-- 바코드 색상 설정 -->
		<p class="lead">바코드 색상 설정</p>
		
		<p><a class="link-primary link-offset-2 link-underline-opacity-25 link-underline-opacity-100-hover" href="https://search.naver.com/search.naver?query=%EC%83%89%EC%83%81%20%ED%8C%94%EB%A0%88%ED%8A%B8" target="_blank">[색상표 검색]</a></p>
		
		<div class="mb-3">
			<small class="text-danger">※기본 사전등록,현장등록 결제자 (파랑),현장등록 (빨강),초청장,샌드포유 (녹색)※</small>
		</div>
		
		<div class="row mb-3 fw-bold text-center">
			<div class="col">
				기본구분
			</div>
			<div class="col">
				기본(#000000)
			</div>
			<div class="col">
				특정일(#000000)
			</div>
		</div>
		<div class="row mb-3">
			<label class="col col-form-label fw-bold" for="bf_color1">사전등록,현장등록 결제자</label>
			<div class="col">
				<input class="form-control" type="text" name="bf_color1" id="bf_color1" maxlength="7" value="${gubunBody.bf_color1 }" ${not empty gubunBody.bf_color1 ? 'style="border: 5px solid; border-color: ' += gubunBody.bf_color1 += ';"' : '' }>
			</div>
			<div class="col">
				<input class="form-control" type="text" name="bf_color2" id="bf_color2" maxlength="7" value="${gubunBody.bf_color2 }" ${not empty gubunBody.bf_color2 ? 'style="border: 5px solid; border-color: ' += gubunBody.bf_color2 += ';"' : '' }>
			</div>
		</div>
		<div class="row mb-3">
			<label class="col col-form-label fw-bold" for="in_color1">현장등록</label>
			<div class="col">
				<input class="form-control" type="text" name="in_color1" id="in_color1" maxlength="7" ${not empty gubunBody.in_color1 ? 'style="border: 5px solid; border-color: ' += gubunBody.in_color1 += ';"' : '' }>
			</div>
			<div class="col">
				<input class="form-control" type="text" name="in_color2" id="in_color2" maxlength="7" ${not empty gubunBody.in_color2 ? 'style="border: 5px solid; border-color: ' += gubunBody.in_color2 += ';"' : '' }>
			</div>
		</div>
		<div class="row mb-3">
			<label class="col col-form-label fw-bold" for="iv_color1">초청장,샌드포유</label>
			<div class="col">
				<input class="form-control" type="text" name="iv_color1" id="iv_color1" maxlength="7" ${not empty gubunBody.iv_color1 ? 'style="border: 5px solid; border-color: ' += gubunBody.iv_color1 += ';"' : '' }>
			</div>
			<div class="col">
				<input class="form-control" type="text" name="iv_color2" id="iv_color2" maxlength="7" ${not empty gubunBody.iv_color2 ? 'style="border: 5px solid; border-color: ' += gubunBody.iv_color2 += ';"' : '' }>
			</div>
		</div>
		<!-- 바코드 색상 설정 -->
		
		<!-- Surv 강제 바코드 색상 설정 -->
		<p class="lead">Surv 강제 바코드 색상 설정</p>
		
		<div class="row mb-3 fw-bold text-center">
			<div class="col">
				<select name="BC_SURV" class="form-select fw-bold">
					<c:forEach var="i" begin="${init.IN_DN ne 'Y' ? 1 : 2 }" end="16">
					<c:set var="BC_SURV" value="Surv${i }" />
						<option value="Surv${i }" ${gubunBody.BC_SURV eq BC_SURV ? 'selected' : '' }>Surv${i }</option>
					</c:forEach>
				</select>
			</div>
			<div class="col">
				타이틀
			</div>
			<div class="col">
				기본(#000000)
			</div>
			<div class="col">
				특정일(#000000)
			</div>
		</div>
		<c:forEach var="i" begin="1" end="5">
		<c:set var="s_code" value="S${i }_code" />
		<c:set var="s_title" value="S${i }_title" />
		<c:set var="s_color1" value="S${i }_color1" />
		<c:set var="s_color2" value="S${i }_color2" />
		<c:set var="s_content" value="S${i }_content" />
		
		<div class="row mb-3">
			<div class="col">
				<input type="text" class="form-control fw-bold" name="S${i }_code" maxlength="1" value="${gubunBody[s_code] }">
			</div>
			<div class="col">
				<input type="text" class="form-control fw-bold" name="S${i }_title" maxlength="100" value="${gubunBody[s_title] }">
			</div>
			<div class="col">
				<input type="text" class="form-control" name="S${i }_color1" maxlength="7" value="${gubunBody[s_color1] }" ${not empty gubunBody[s_color1] ? 'style="border: 5px solid; border-color: ' += gubunBody[s_color1] += ';"' : '' }>
			</div>
			<div class="col">
				<input type="text" class="form-control" name="S${i }_color2" maxlength="7" value="${gubunBody[s_color2] }" ${not empty gubunBody[s_color2] ? 'style="border: 5px solid; border-color: ' += gubunBody[s_color2] += ';"' : '' }>
			</div>
		</div>
		<div class="row mb-3 pb-3 border-3 border-bottom border-dark">
			<div class="col-3">
				<label class="col-form-label fw-bold" for="S${i }_content">안내문구</label>
			</div>
			<div class="col-9">
				<textarea class="form-control fw-bold" rows="5" name="S${i }_content" id="S${i }_content">${gubunBody[s_content] }</textarea>
			</div>
		</div>
		</c:forEach>
		<!-- Surv 강제 바코드 색상 설정 -->
		
		<!-- 구분번호 강제 바코드 색상 설정 -->
		<p class="lead">구분번호 강제 바코드 색상 설정</p>
		
		<div class="row mb-3 fw-bold text-center">
			<div class="col">
				등록번호 구분(72)
			</div>
			<div class="col">
				타이틀
			</div>
			<div class="col">
				기본(#000000)
			</div>
			<div class="col">
				특정일(#000000)
			</div>
		</div>
		
		<c:forEach var="i" begin="1" end="10">
		<c:set var="g_code" value="G${i }_code" />
		<c:set var="g_title" value="G${i }_title" />
		<c:set var="g_color1" value="G${i }_color1" />
		<c:set var="g_color2" value="G${i }_color2" />
		<c:set var="g_content" value="G${i }_content" />
		
		<div class="row mb-3">
			<div class="col">
				<input type="text" class="form-control fw-bold" name="G${i }_code" maxlength="2" value="${gubunBody[g_code] }">
			</div>
			<div class="col">
				<input type="text" class="form-control fw-bold" name="G${i }_title" maxlength="100" value="${gubunBody[g_title] }">
			</div>
			<div class="col">
				<input type="text" class="form-control" name="G${i }_color1" maxlength="7" value="${gubunBody[g_color1] }" ${not empty gubunBody[g_color1] ? 'style="border: 5px solid; border-color: ' += gubunBody[g_color1] += ';"' : '' }>
			</div>
			<div class="col">
				<input type="text" class="form-control" name="G${i }_color2" maxlength="7" value="${gubunBody[g_color2] }" ${not empty gubunBody[g_color2] ? 'style="border: 5px solid; border-color: ' += gubunBody[g_color2] += ';"' : '' }>
			</div>
		</div>
		<div class="row mb-3 pb-3 border-3 border-bottom border-dark">
			<div class="col-3">
				<label class="col-form-label fw-bold" for="G${i }_content">안내문구</label>
			</div>
			<div class="col-9">
				<textarea class="form-control fw-bold" name="G${i }_content" id="G${i }_content" rows="5">${gubunBody[g_content] }</textarea>
			</div>
		</div>
		</c:forEach>
		<!-- 구분번호 강제 바코드 색상 설정 -->
		
		<input type="submit" class="btn btn-primary" value="저장" onclick="gubunSave();">
	</form>

<hr>
<script>
	function gubunSave() {
		var formData = new FormData(document.getElementById("frm"));
		var arrSPCDATE = [];
		
		$("input[name=SPCDATE]").each(function(i) {
			arrSPCDATE[i] = this.checked ? this.value : "false";
		});
		
		formData.append("arrSPCDATE", arrSPCDATE);
		
		$.ajax({
			url: '<c:url value="/dev/gubun_ok" />',
			type: 'post',
			data: formData,
			processData : false,
			contentType : false,
			cache : false,
			success: function(data) {
				if (data.success) {
					alert("ok");
					location.href = "<c:url value='/dev/gubun' />";
				} else {
					alert("설정 확인 후 다시 시도하시기 바랍니다.");
				}
			},
			error: function(req, stat, err) {
				console.log(err);
			}
		});
	}
</script>