<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<hr>
<form name="frm" id="frm" method="post" action="<c:url value='/dev/info_ok' />" onsubmit="return false;">
	<input type="hidden" name="INCD" value="asdf8515">
	
	<p class="lead fw-bold my-3">기본 수집 설정</p>
	
	<div class="row mb-3 fw-bold text-center">
		<div class="col">
			구분
		</div>
		<div class="col">
			설정
		</div>
		<div class="col">
			등록구분별 미사용 체크
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Name2_use_div" class="col col-form-label fw-bold">성명 영문(MiddleName)</label>
		<div class="col" id="Name2_use_div">
			<input class="form-check-input" type="checkbox" name="Name2_use" id="Name2_use" value="Y" ${info.Name2_use eq 'Y' ? 'checked' : '' }>
			<label for="Name2_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Name2_req" id="Name2_req" value="Y" ${info.Name2_req eq 'Y' ? 'checked' : '' }>
			<label for="Name2_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Name2_use_N" id="Name2_use_N_in" value="in" ${info.Name2_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Name2_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Name2_use_N" id="Name2_use_N_iv" value="iv" ${info.Name2_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Name2_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Name2_use_N" id="Name2_use_N_bf" value="bf" ${info.Name2_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Name2_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Name2_use_N" id="Name2_use_N_sd" value="sd" ${info.Name2_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Name2_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Name2_use_N" id="Name2_use_N_co" value="co" ${info.Name2_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Name2_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Birthday_use" class="col col-form-label fw-bold">생년월일(LastName)</label>
		<div class="col">
			<input class="form-check-input" type="checkbox" name="Birthday_use" id="Birthday_use" value="Y" ${info.Birthday_use eq 'Y' ? 'checked' : '' }>
			<label for="Birthday_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Birthday_req" id="Birthday_req" value="Y" ${info.Birthday_req eq 'Y' ? 'checked' : '' }>
			<label for="Birthday_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Birthday_use_N" id="Birthday_use_N_in" value="in" ${info.Birthday_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Birthday_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Birthday_use_N" id="Birthday_use_N_iv" value="iv" ${info.Birthday_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Birthday_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Birthday_use_N" id="Birthday_use_N_bf" value="bf" ${info.Birthday_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Birthday_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Birthday_use_N" id="Birthday_use_N_sd" value="sd" ${info.Birthday_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Birthday_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Birthday_use_N" id="Birthday_use_N_co" value="co" ${info.Birthday_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Birthday_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Organization_use_div" class="col col-form-label fw-bold">회사명(Orgn1)</label>
		<div class="col" id="Organization_use_div">
			<input class="form-check-input" type="checkbox" name="Organization_use" id="Organization_use" value="Y" ${info.Organization_use eq 'Y' ? 'checked' : '' }>
			<label for="Organization_use" class="form-check-label">사용</label>
			(<input class="form-check-input" type="radio" name="Organization_req" id="Organization_req" value="Y" ${info.Organization_req eq 'Y' ? 'checked' : '' } checked>
			<label for="Organization_req" class="form-check-label">필수</label>
			<input class="form-check-input" type="radio" name="Organization_req" id="Organization_req_S" value="S" ${info.Organization_req eq 'S' ? 'checked' : '' }>
			<label for="Organization_req_S" class="form-check-label">회사없음</label>
			<input class="form-check-input" type="radio" name="Organization_req" id="Organization_req_N" value="N" ${info.Organization_req eq 'N' ? 'checked' : '' }>
			<label for="Organization_req_N" class="form-check-label">선택</label>)
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Organization_use_N" id="Organization_use_N_in" value="in" ${info.Organization_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Organization_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Organization_use_N" id="Organization_use_N_iv" value="iv" ${info.Organization_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Organization_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Organization_use_N" id="Organization_use_N_bf" value="bf" ${info.Organization_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Organization_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Organization_use_N" id="Organization_use_N_sd" value="sd" ${info.Organization_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Organization_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Organization_use_N" id="Organization_use_N_co" value="co" ${info.Organization_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Organization_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Organization2_use_div" class="col col-form-label fw-bold">회사명 영문(Orgn2)</label>
		<div class="col" id="Organization2_use_div">
			<input class="form-check-input" type="checkbox" name="Organization2_use" id="Organization2_use" value="Y" ${info.Organization2_use eq 'Y' ? 'checked' : '' }>
			<label for="Organization2_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Organization2_req" id="Organization2_req" value="Y" ${info.Organization2_req eq 'Y' ? 'checked' : '' }>
			<label for="Organization2_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Organization2_use_N" id="Organization2_use_N_in" value="in" ${info.Organization2_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Organization2_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Organization2_use_N" id="Organization2_use_N_iv" value="iv" ${info.Organization2_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Organization2_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Organization2_use_N" id="Organization2_use_N_bf" value="bf" ${info.Organization2_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Organization2_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Organization2_use_N" id="Organization2_use_N_sd" value="sd" ${info.Organization2_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Organization2_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Organization2_use_N" id="Organization2_use_N_co" value="co" ${info.Organization2_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Organization2_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Department_use_div" class="col col-form-label fw-bold">부서(Part)</label>
		<div class="col" id="Department_use_div">
			<input class="form-check-input" type="checkbox" name="Department_use" id="Department_use" value="Y" ${info.Department_use eq 'Y' ? 'checked' : '' }>
			<label for="Department_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Department_req" id="Department_req" value="Y" ${info.Department_req eq 'Y' ? 'checked' : '' }>
			<label for="Department_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Department_use_N" id="Department_use_N_in" value="in" ${info.Department_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Department_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Department_use_N" id="Department_use_N_iv" value="iv" ${info.Department_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Department_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Department_use_N" id="Department_use_N_bf" value="bf" ${info.Department_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Department_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Department_use_N" id="Department_use_N_sd" value="sd" ${info.Department_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Department_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Department_use_N" id="Department_use_N_co" value="co" ${info.Department_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Department_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Position_use_div" class="col col-form-label fw-bold">직급(Rank)</label>
		<div class="col" id="Position_use_div">
			<input class="form-check-input" type="checkbox" name="Position_use" id="Position_use" value="Y" ${info.Position_use eq 'Y' ? 'checked' : '' }>
			<label for="Position_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Position_req" id="Position_req" value="Y" ${info.Position_req eq 'Y' ? 'checked' : '' }>
			<label for="Position_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Position_use_N" id="Position_use_N_in" value="in" ${info.Position_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Position_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Position_use_N" id="Position_use_N_iv" value="iv" ${info.Position_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Position_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Position_use_N" id="Position_use_N_bf" value="bf" ${info.Position_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Position_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Position_use_N" id="Position_use_N_sd" value="sd" ${info.Position_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Position_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Position_use_N" id="Position_use_N_co" value="co" ${info.Position_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Position_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Addr_use_div" class="col col-form-label fw-bold">주소(Addr1,Addr2)</label>
		<div class="col" id="Addr_use_div">
			<input class="form-check-input" type="checkbox" name="Addr_use" id="Addr_use" value="Y" ${info.Addr_use eq 'Y' ? 'checked' : '' }>
			<label for="Addr_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Addr_req" id="Addr_req" value="Y" ${info.Addr_req eq 'Y' ? 'checked' : '' }>
			<label for="Addr_req" class="form-check-label">필수</label>
			<input class="form-check-input" type="checkbox" name="Addr2_use" id="Addr2_use" value="Y" ${info.Addr2_use eq 'Y' ? 'checked' : '' }>
			<label for="Addr2_use" class="form-check-label">상세포함</label>
			<br>
			<div class="form-check form-check-inline">
				(<input type="radio" name="Addr_put" id="Addr_put" value="A" ${info.Addr_put eq 'A' ? 'checked' : '' } checked>
				<label for="Addr_put" class="form-check-label">기본</label>
			</div>
			<div class="form-check form-check-inline">
				<input type="radio" name="Addr_put" id="Addr_put2" value="S" ${info.Addr_put eq 'S' ? 'checked' : '' }>
				<label for="Addr_put2" class="form-check-label">지역분류</label>)
			</div>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Addr_use_N" id="Addr_use_N_in" value="in" ${info.Addr_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Addr_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Addr_use_N" id="Addr_use_N_iv" value="iv" ${info.Addr_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Addr_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Addr_use_N" id="Addr_use_N_bf" value="bf" ${info.Addr_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Addr_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Addr_use_N" id="Addr_use_N_sd" value="sd" ${info.Addr_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Addr_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Addr_use_N" id="Addr_use_N_co" value="co" ${info.Addr_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Addr_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Email_use_div" class="col col-form-label fw-bold">이메일(Email)</label>
		<div class="col" id="Email_use_div">
			<input class="form-check-input" type="checkbox" name="Email_use" id="Email_use" value="Y" ${info.Email_use eq 'Y' ? 'checked' : '' }>
			<label for="Email_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Email_req" id="Email_req" value="Y" ${info.Email_req eq 'Y' ? 'checked' : '' }>
			<label for="Email_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Email_use_N" id="Email_use_N_in" value="in" ${info.Email_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Email_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Email_use_N" id="Email_use_N_iv" value="iv" ${info.Email_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Email_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Email_use_N" id="Email_use_N_bf" value="bf" ${info.Email_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Email_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Email_use_N" id="Email_use_N_sd" value="sd" ${info.Email_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Email_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Email_use_N" id="Email_use_N_co" value="co" ${info.Email_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Email_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Phone_use_div" class="col col-form-label fw-bold">전화(Phone)</label>
		<div class="col" id="Phone_use_div">
			<input class="form-check-input" type="checkbox" name="Phone_use" id="Phone_use" value="Y" ${info.Phone_use eq 'Y' ? 'checked' : '' }>
			<label for="Phone_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Phone_req" id="Phone_req" value="Y" ${info.Phone_req eq 'Y' ? 'checked' : '' }>
			<label for="Phone_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Phone_use_N" id="Phone_use_N_in" value="in" ${info.Phone_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Phone_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Phone_use_N" id="Phone_use_N_iv" value="iv" ${info.Phone_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Phone_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Phone_use_N" id="Phone_use_N_bf" value="bf" ${info.Phone_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Phone_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Phone_use_N" id="Phone_use_N_sd" value="sd" ${info.Phone_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Phone_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Phone_use_N" id="Phone_use_N_co" value="co" ${info.Phone_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Phone_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Fax_use_div" class="col col-form-label fw-bold">팩스(Fax)</label>
		<div class="col" id="Fax_use_div">
			<input class="form-check-input" type="checkbox" name="Fax_use" id="Fax_use" value="Y" ${info.Fax_use eq 'Y' ? 'checked' : '' }>
			<label for="Fax_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Fax_req" id="Fax_req" value="Y" ${info.Fax_req eq 'Y' ? 'checked' : '' }>
			<label for="Fax_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Fax_use_N" id="Fax_use_N_in" value="in" ${info.Fax_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Fax_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Fax_use_N" id="Fax_use_N_iv" value="iv" ${info.Fax_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Fax_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Fax_use_N" id="Fax_use_N_bf" value="bf" ${info.Fax_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Fax_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Fax_use_N" id="Fax_use_N_sd" value="sd" ${info.Fax_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Fax_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Fax_use_N" id="Fax_use_N_co" value="co" ${info.Fax_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Fax_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<div class="row mb-3">
		<label for="Homepage_use_div" class="col col-form-label fw-bold">웹사이트(Url)</label>
		<div class="col" id="Homepage_use_div">
			<input class="form-check-input" type="checkbox" name="Homepage_use" id="Homepage_use" value="Y" ${info.Homepage_use eq 'Y' ? 'checked' : '' }>
			<label for="Homepage_use" class="form-check-label">사용</label>
			<input class="form-check-input" type="checkbox" name="Homepage_req" id="Homepage_req" value="Y" ${info.Homepage_req eq 'Y' ? 'checked' : '' }>
			<label for="Homepage_req" class="form-check-label">필수</label>
		</div>
		<div class="col">
			<input type="checkbox" class="form-check-input" name="Homepage_use_N" id="Homepage_use_N_in" value="in" ${info.Homepage_use_N[0] eq 'in' ? 'checked' : '' }>
			<label for="Homepage_use_N_in" class="form-check-label">in</label>
			<input type="checkbox" class="form-check-input" name="Homepage_use_N" id="Homepage_use_N_iv" value="iv" ${info.Homepage_use_N[1] eq 'iv' ? 'checked' : '' }>
			<label for="Homepage_use_N_iv" class="form-check-label">iv</label>
			<input type="checkbox" class="form-check-input" name="Homepage_use_N" id="Homepage_use_N_bf" value="bf" ${info.Homepage_use_N[2] eq 'bf' ? 'checked' : '' }>
			<label for="Homepage_use_N_bf" class="form-check-label">bf</label>
			<input type="checkbox" class="form-check-input" name="Homepage_use_N" id="Homepage_use_N_sd" value="sd" ${info.Homepage_use_N[3] eq 'sd' ? 'checked' : '' }>
			<label for="Homepage_use_N_sd" class="form-check-label">sd</label>
			<input type="checkbox" class="form-check-input" name="Homepage_use_N" id="Homepage_use_N_co" value="co" ${info.Homepage_use_N[4] eq 'co' ? 'checked' : '' }>
			<label for="Homepage_use_N_co" class="form-check-label">co</label>
		</div>
	</div>
	
	<button type="button" class="btn btn-primary" onclick="infoSave();">저장</button>
</form>
<hr>
<script>
	function infoSave() {
		$.ajax({
			type : 'post',
			url : '<c:url value="/dev/info_ok" />',
			data : JSON.stringify($("#frm").serialize()),
			success : function(data) {
				if (data.success) {
					alert("ok");
					location.href = "<c:url value='/dev/info' />";
				} else {
					alert("설정 확인 후 다시 시도하시기 바랍니다.");
				}
			},
			error : function(request, status, error) {
				console.log(error);
			}
		});
	}
</script>