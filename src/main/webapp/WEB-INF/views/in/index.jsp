<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<style>
.nav-pills .nav-link.active { background-color: var(--bs-secondary-color); }
</style>
<div id="site" class="h-100 d-flex align-items-center justify-content-center pb-4">
	<div class="card">
		<img src="<c:url value='/upload/images/${initVO.LOGO_FILE }' />" class="card-img-top">
		<c:if test="${initVO.IN_LANG eq 'Y' }">
		<div class="card-body">
			<ul class="nav nav-pills justify-content-center" id="pills-tab" role="tablist">
				<li class="nav-item" role="presentation">
					<button class="nav-link ${pageContext.response.locale eq 'ko' ? 'active' : '' }" id="pills-koreaa-tab" data-bs-toggle="pill" type="button" role="tab" aria-controls="pills-korean" aria-selected="true">KOREAN</button>
				</li>
				<li class="nav-item" role="presentation">
					<button class="nav-link ${pageContext.response.locale eq 'en' ? 'active' : '' }" id="pills-english-tab" data-bs-toggle="pill" type="button" role="tab" aria-controls="pills-english" aria-selected="false">ENGLISH</button>
				</li>
			</ul>
		</div>
		</c:if>
		<div class="card-body">
			<form>
				<div class="mb-3">
					<label for="name" class="form-label"><spring:message code="site.name" /></label>
					<input type="text" class="form-control" id="name" name="name" v-model.trim="name" placeholder="<spring:message code='site.name.placeholder' />" value="" maxlength="64">
				</div>
				<div class="mb-3">
					<label for="mobile" class="form-label"><spring:message code="site.mobile" /></label>
					<input type="text" class="form-control" id="mobile" name="mobile" v-model.trim="mobile" placeholder="<spring:message code='site.mobile.placeholder' />" value="" maxlength="128">
				</div>
				<div class="d-flex flex-column">
					<button type="button" class="btn btn-danger btn-lg mt-3" @click="reg_ck">
						<spring:message code="site.regist" />
					</button>
				</div>
			</form>
		</div>
		<div class="card-footer text-body-secondary text-center">
			Copyright ⓒ SYSFORU INC. All Rights Reserved
		</div>
	</div>
</div>
<script>
	$(function() {
		$("button.nav-link").on("click", function() {
			location.href = "<c:url value='/in/index?lang=" + this.innerText.substring(0, 2).toLowerCase() + "' />";
		});
	});
	
	new Vue({
		el: "#site",
		data: {
			name: '',
			mobile: '',
			lang: "${pageContext.response.locale }",
			BarcodeExhibitionNo: "${initVO.ExhibitionNo }",
		},
		methods: {
			reg_ck() {
				const name = this.name;
				const mobile = this.mobile;
				
				if (!name) {
					alert("<spring:message code='site.name.empty.warning' />");
					$("#name").focus();
					
					return false;
				}
				
				if (!mobile) {
					alert("<spring:message code='site.mobile.empty.warning' />");
					$("#mobile").focus();
					
					return false;
				} else if (!this.mobileChk(mobile)) {
					alert("<spring:message code='site.mobile.not.correctly' />");
					$("#mobile").focus();
					
					return false;
				}
				
				this.check(name, mobile);
			},
			mobileChk(number) {
				const koRule = /^(01[016789]{1})[0-9]{3,4}[0-9]{4}$/;
				const enRule = /^[\+\d{1,}|\d{1,}][\-\s|\d|\(|\)]{0,}[0-9]{4,}$/;
				//const enRule = /^\+[0-9](?:[- ]?[0-9]){,14}$/;
				
				return ${pageContext.response.locale }Rule.test(number);
			},
			check(name, mobile) {
				let param = JSON.stringify({
					name: name,
					mobile: mobile,
					BarcodeExhibitionNo: this.BarcodeExhibitionNo,
				});
				
				axios.post("<c:url value='/dev/duplicateCheck' />", param, {
					headers: {
						"content-Type": "application/json; charset=UTF-8",
					}
				})
				.then(function (response) {
					const result = response.data.result;
					
					if (result > 1 || result === 1) {
						//location.href = "<c:url value='/in/confirm' />";
					} else if (result === 0) {
						location.href = '<c:url value="/in/regist?name=' + encodeURIComponent(name) + '&mobile=' + encodeURIComponent(mobile) + '" />';
					}
				})
				.catch(function (error) {
					console.log(error);
				});
			}
		}
	});
</script>