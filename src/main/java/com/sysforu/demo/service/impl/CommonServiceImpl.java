package com.sysforu.demo.service.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.GsonBuilder;
import com.sysforu.demo.mapper.CommonMapper;
import com.sysforu.demo.service.CommonService;
import com.sysforu.demo.vo.InfoVO;
import com.sysforu.demo.vo.InitVO;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@PropertySource("classpath:application.properties")
public class CommonServiceImpl implements CommonService {
	@Value("${upload.common.init}")
	private String initPath;
	@Value("${upload.common.info}")
	private String infoPath;
	@Value("${upload.common.surv.foot}")
	private String survFootPath;
	@Value("${upload.common.surv.code}")
	private String survCodePath;
	@Value("${upload.common.gubun.body}")
	private String gubunBodyPath;
	@Value("${upload.images}")
	private String imgPath;
	
	@Autowired
	private CommonMapper commonMapper;

	@SuppressWarnings("unchecked")
	@Override
	public Map<String, Object> getInit() {
		Map<String, Object> init = new HashMap<String, Object>();
		try {
			File file = new File(System.getProperty("user.home").concat(initPath));
			Reader reader = new FileReader(file);
			
			JSONParser parser = new JSONParser();
			JSONObject jObj = (JSONObject) parser.parse(reader);
			
			Iterator<String> iter = jObj.keySet().iterator();
			while (iter.hasNext()) {
				String key = iter.next();
				init.put(key, jObj.get(key));
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return init;
	}
	
	@Override
	public Map<String, Object> getInfo() {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		Map<String, Object> info = new HashMap<String, Object>();
		
		try {
			FileReader reader = new FileReader(System.getProperty("user.home").concat(infoPath));
			JSONObject jObj = (JSONObject) parser.parse(reader);
			reader.close();
			
			Iterator<?> iter = jObj.keySet().iterator();
			while (iter.hasNext()) {
				String key = String.valueOf(iter.next());
				info.put(key, jObj.get(key));
			}
			
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return info;
	}
	
	@Override
	public Map<String, Object> getSurv() {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		Map<String, Object> surv = new HashMap<String, Object>();
		
		try {
			FileReader reader = new FileReader(System.getProperty("user.home").concat(survFootPath));
			JSONObject jObj = (JSONObject) parser.parse(reader);
			reader.close();
			
			Iterator<?> iter = jObj.keySet().iterator();
			while (iter.hasNext()) {
				String key = String.valueOf(iter.next());
				surv.put(key, jObj.get(key));
			}
			
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return surv;
	}
	
	@Override
	public List<Map<String, Object>> selectColumnT() {
		// TODO Auto-generated method stub
		return commonMapper.selectColumnT();
	}
	
	@Override
	public List<Map<String, Object>> selectSurvT() {
		// TODO Auto-generated method stub
		return commonMapper.selectSurvT();
	}
	
	@Override
	public String getSurvCode() {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		
		try {
			FileReader reader = new FileReader(System.getProperty("user.home").concat(survCodePath));
			JSONArray jArr = (JSONArray) parser.parse(reader);
			reader.close();
			
			return jArr.toJSONString();
		} catch (IOException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return "error";
	}
	
	@Override
	public Map<String, Object> getGubun() {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		Map<String, Object> gubun = new HashMap<String, Object>();
		
		try {
			FileReader reader = new FileReader(System.getProperty("user.home").concat(gubunBodyPath));
			JSONObject jObj = (JSONObject) parser.parse(reader);
			reader.close();
			
			Iterator<?> iter = jObj.keySet().iterator();
			while (iter.hasNext()) {
				String key = (String) iter.next();
				gubun.put(key, jObj.get(key));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return gubun;
	}
	
	@Override
	public Map<String, Object> setInit(InitVO init) {
		Map<String, Object> result = new HashMap<String, Object>();
		int rs = 0;
		result.put("success", "false");
		String path = System.getProperty("user.home");
		
		for (MultipartFile mf : init.getLOGO_UP()) {
			if (!mf.isEmpty()) {
				try {
					String originalFileName = mf.getOriginalFilename();
					init.setLOGO_FILE(originalFileName);
					mf.transferTo(new File(path.concat(imgPath), originalFileName));
					rs++;
				} catch (IllegalStateException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		try {
			FileWriter jsonWriter = new FileWriter(path.concat(initPath));
			jsonWriter.write(new GsonBuilder()
					.setPrettyPrinting()
					.create()
					.toJson(init)
					.toString());
			jsonWriter.flush();
			jsonWriter.close();
			rs++;
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (rs > 0) {
			result.replace("success", "true");
		}
		
		return result;
	}
	
	@Override
	public Map<String, Object> setInfo(InfoVO info) {
		// TODO Auto-generated method stub
		String path = System.getProperty("user.home");
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", "false");
		
		try {
			FileWriter infoWriter = new FileWriter(path.concat(infoPath));
			infoWriter.write(new GsonBuilder()
					.setPrettyPrinting()
					.create()
					.toJson(info)
					.replace("\\\"", "")
					.toString());
			infoWriter.flush();
			infoWriter.close();
			result.put("success", "true");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", "false");
		}
		
		return result;
		
	}
	
	@Override
	public Map<String, Object> setSurv(HttpServletRequest request) {
		// TODO Auto-generated method stub
		Map<String, Object> rMap = new HashMap<String, Object>();
		String path = System.getProperty("user.home");
		rMap.put("success", false);
		
		Iterator<String> iter = request.getParameterMap().keySet().iterator();
		
		SortedMap<String, Object> survFootMap = new TreeMap<String, Object>();
		SortedMap<String, Object> survCodeMap = new TreeMap<String, Object>();
		
		List<Map<String, Object>> testList = new ArrayList<>();
		
		while (iter.hasNext()) {
			String key = iter.next();
			survFootMap.put(key, request.getParameter(key));
			
			if (key.matches("(.*)_Use_N")) {
				List<String> survList = new ArrayList<String>();
				for (String s : request.getParameterValues(key)) {
					survList.add(s);
				}
				survFootMap.replace(key, survList);
			}
			
			if (key.matches("(.*)_Data")) {
				if (!request.getParameter(key).isEmpty()) {
					String survNum = key.substring(0, key.indexOf("_"));
					String[] readDataLine = request.getParameter(key).trim()
							.split(System.getProperty("line.separator"));
					
					List<Map<String, Object>> dataList = new ArrayList<>();
					
					for (String readLine : readDataLine) {
						String[] splitData = readLine.trim().split("\\|");
						/*Map<String, Object> data = new HashMap<>();
						
						if (survNum.startsWith("S")) {
							data.put("Data", new String[] {splitData[0], splitData[1]});
							data.put("Data_en", new String[] {splitData[0], splitData[2]});
						} else {
							data.put("Data", new String[] {splitData[0], splitData[1]});
						}
						
						dataList.add(data);*/
						
						Map<String, Object> testMap = new HashMap<String, Object>();
						if (survNum.startsWith("S")) {
							testMap.put(survNum, "[{\"Data\" : \"" + splitData[0] + "\", \"Data_en\" : \"" + splitData[0] + "\"}, {\"Data\" : \"" + splitData[1] + "\", \"Data_en\" : \"" + splitData[2] + "\"}]");
						} else {
							testMap.put(survNum, "[{\"Data\" : \"" + splitData[0] + "\"}, {\"Data\" : \"" + splitData[1] + "\"}]");
						}
						testList.add(testMap);
					} // .end forEach
					//survCodeMap.put(survNum, dataList);
				}
			} // .end if
		} // .end while
		
		try {
			FileWriter survFootWriter = new FileWriter(path.concat(survFootPath));
			FileWriter survCodeWriter = new FileWriter(path.concat(survCodePath));
			survFootWriter.write(new GsonBuilder()
					.setPrettyPrinting()
					.create()
					.toJson(survFootMap)
					.replace("\\\"", "")
					.toString());
			survFootWriter.flush();
			survFootWriter.close();
			
			survCodeWriter.write(new GsonBuilder()
					.setPrettyPrinting()
					.disableHtmlEscaping()
					.create()
					//.toJson(survCodeMap)
					.toJson(testList)
					//.replace("\\\"", "")
					.replace("\\", "")
					.replace("\"[", "[")
					.replace("]\"", "]")
					.toString());
			survCodeWriter.flush();
			survCodeWriter.close();
			
			rMap.put("success", "true");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			rMap.put("success", "false");
		}
		
		return rMap;
	}
	
	@Override
	public Map<String, Object> setGubun(HttpServletRequest request) {
		// TODO Auto-generated method stub
		Map<String, Object> gubunMap = new HashMap<String, Object>();
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		String rootPath = System.getProperty("user.home");
		Map<?, ?> map = request.getParameterMap();
		for (Object key : map.keySet()) {
			String keyStr = (String) key;
			
			gubunMap.put(keyStr, request.getParameter(keyStr));
		}
		
		try {
			FileWriter gubunWirter = new FileWriter(rootPath.concat(gubunBodyPath));
			gubunWirter.write(new GsonBuilder()
					.setPrettyPrinting()
					.create()
					.toJson(gubunMap)
					.replace("\\\"", "")
					.toString());
			gubunWirter.flush();
			gubunWirter.close();
			result.put("success", true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		
		return result;
	}
	
	@Override
	public Map<String, Object> setNdata(Map<String, Object> entity) {
		// TODO Auto-generated method stub
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("success", false);
		
		try {
			JSONParser jsonParser = new JSONParser();
			JSONObject footJson = (JSONObject) jsonParser.parse((String) entity.get("foot"));
			JSONArray codeArray = (JSONArray) jsonParser.parse((String) entity.get("code"));
			List<Map<String, Object>> insertList = new ArrayList<Map<String, Object>>();
			for (int i = 0; i < codeArray.size(); i++) {
				JSONObject inJson = (JSONObject) codeArray.get(i);
				
				for (Object obj : inJson.keySet()) {
					String inKey = (String) obj;
					
					if (footJson.get(inKey.concat("_Use")).equals("Y")) {
						commonMapper.deleteSurveyT(inKey.replace("Surv", ""));
						JSONArray inArray = (JSONArray) inJson.get(inKey);
						JSONObject survc = (JSONObject) inArray.get(0);
						JSONObject survs = (JSONObject) inArray.get(1);
						Map<String, Object> paramMap = new HashMap<String, Object>();
						
						paramMap.put("use", true);
						paramMap.put("SurveyN", inKey.replace("Surv", ""));
						paramMap.put("SurveyC", survc.get("Data"));
						paramMap.put("SurveyS", survs.get("Data"));
						insertList.add(paramMap);
					}
					
				} // .end forEach
			} // .end for
			
			for (Map<String, Object> map : insertList) {
				if ((boolean) map.get("use")) {
					commonMapper.insertSurveyT(map);
				}
			}
			result.put("success", true);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result.put("success", false);
		}
		
		return result;
	}

	@Override
	public Map<String, Object> selectExhibitT(Map<String, Object> initMap) {
		// TODO Auto-generated method stub
		Map<String, Object> map = commonMapper.selectExhibitT(initMap);
		List<String> dateList = new ArrayList<String>();
		Timestamp tsFrom = (Timestamp) map.get("FromDate");
		Timestamp tsTo = (Timestamp) map.get("ToDate");
		String fromDate = new SimpleDateFormat("yyyy-MM-dd").format(tsFrom);
		String toDate = new SimpleDateFormat("yyyy-MM-dd").format(tsTo);
		map.put("between", ChronoUnit.DAYS.between(LocalDate.parse(fromDate), LocalDate.parse(toDate)));
		
		long cnt = (long) map.get("between");
		for (int i = 0; i <= cnt; i++) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(tsFrom);
			cal.add(Calendar.DATE, i);
			dateList.add(new SimpleDateFormat("yyyy-MM-dd").format(cal.getTime()));
		}
		map.put("dateList", dateList);
		
		return map;
	}
	
	@Override
	public Map<String, Object> duplicateCheck(Map<String, Object> entity) {
		// TODO Auto-generated method stub
		Map<String, Object> resultMap = new HashMap<String, Object>();
		entity.replace("name", entity.get("name").toString().toUpperCase());
		entity.replace("mobile", entity.get("mobile").toString().replaceAll("[^0-9]", ""));
		
		resultMap.put("result", commonMapper.duplicateCheck(entity));
		
		return resultMap;
	}
}
