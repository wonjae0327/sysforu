<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>${initVO.titleStr }</title>

<script type="text/javascript" src="/webjars/jquery/3.7.1/dist/jquery.min.js"></script>
<script type="text/javascript" src="/webjars/bootstrap/5.3.2/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/webjars/bootstrap-datepicker/1.10.0/dist/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="/webjars/bootstrap-datepicker/1.10.0/dist/locales/bootstrap-datepicker.ko.min.js"></script>
<script type="text/javascript" src="/webjars/bootstrap-datepicker/1.10.0/dist/locales/bootstrap-datepicker.en-US.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vue@2.7.16"></script>
<script type="text/javascript" src="/webjars/axios/1.6.7/dist/axios.js"></script>
<script src="//t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<link rel="stylesheet" href="/webjars/bootstrap/5.3.2/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/webjars/bootstrap-icons/1.11.3/font/bootstrap-icons.min.css">
<link rel="stylesheet" href="/webjars/bootstrap-datepicker/1.10.0/dist/css/bootstrap-datepicker3.min.css">
<style type="text/css">
html, body {
	height: 100%;
}

/*#registFrm fieldset:not(:first-of-type) {
	display: none;
}*/

.col-form-label.required:after {
	content: "*";
	color: #FF6600;
	font-size: 25px;
	vertical-align: -webkit-baseline-middle;
	line-height: 0;
}
</style>
</head>
<body>

	<tiles:insertAttribute name="body" />
	
</body>
</html>